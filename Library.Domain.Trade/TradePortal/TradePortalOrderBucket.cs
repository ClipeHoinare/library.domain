﻿using System.Collections.Generic;

namespace Library.Domain.TradePortal
{
    public partial class TradePortalOrderBucket //: TradePortalAbstract
    {
        public TradePortalContract Contract { get; set; }
        public TradePortalOrder Order { get; set; }
    }

    public partial class TradePortalContract
    {
        public string Symbol { get; set; }
        public string SecType { get; set; }
        public string Currency { get; set; }
        public string Exchange { get; set; }
        public string LocalSymbol { get; set; }
        public string TradingClass { get; set; }
        public int ConId { get; set; }

        public List<TradePortalComboLeg> ComboLegs { get; set; }
    }
    
    public partial class TradePortalContractBase
    {
        public string Symbol { get; set; }
        public string SecType { get; set; }
        public string Currency { get; set; }
        public string Exchange { get; set; }
        public string LocalSymbol { get; set; }
        public string TradingClass { get; set; }
        public int ConId { get; set; }
    }

    public partial class TradePortalOrder
    {
        public string Action { get; set; }
        public string OrderType { get; set; }
        public decimal LmtPrice { get; set; }
        public long TotalQuantity { get; set; }
        public string Account { get; set; }
        public string Tif { get; set; }
    }
    public partial class TradePortalComboLeg
    {
        public string Action { get; set; }
        public string Exchange { get; set; }
        public string LastTradeDateOrContractMonth { get; set; }
        public int ConId { get; set; }
        public int Ratio { get; set; }
        public string Right { get; set; }
        public string SecType { get; set; }
        public decimal Strike { get; set; }
        public string LocalSymbol { get; set; }
    }
}