﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Domain.TradePortal
{
    public class TradePortalHabitat
    {
        public enum HabitatFlag
        {
            Sandbox = 0,
            Live = 1,
            PaperMoney = 2
        }

        public static HabitatFlag InterpretFlag(string habitatString)
        {
            HabitatFlag habitatFlag;

            if (habitatString.ToLower().Equals(HabitatFlag.Live.ToString().ToLower()))
            {
                habitatFlag = HabitatFlag.Live;
            }
            else if (habitatString.ToLower().Equals(HabitatFlag.PaperMoney.ToString().ToLower()))
            {
                habitatFlag = HabitatFlag.PaperMoney;
            }
            else if (habitatString.ToLower().Equals(HabitatFlag.Sandbox.ToString().ToLower()))
            {
                habitatFlag = HabitatFlag.Sandbox;
            }
            else
            {
                throw new Exception(string.Format("Unknown habitat: %0", habitatString));
            }

            return habitatFlag;
        }
    }
}
