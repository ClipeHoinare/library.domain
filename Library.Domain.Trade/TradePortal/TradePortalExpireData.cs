﻿namespace Library.Domain.TradePortal
{
    public class TradePortalExpireDates : TradePortalAbstract
    {
        public string TradeDate { set; get; }
        public string ExpiryType { set; get; }
    }
}
