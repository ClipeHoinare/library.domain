﻿namespace Library.Domain.TradePortal
{
    public partial class TradePortalStockData : TradePortalAbstract
    {
        public string Symbol { get; set; }
        public string DESCRIPTION_INDEX { get; set; }
        public string MARKET_DATA_TYPE_INDEX { get; set; }
        public decimal Bid { get; set; }
        public bool PRE_OPEN_BID { get; set; }
        public decimal Ask { get; set; }
        public bool PRE_OPEN_ASK { get; set; }
        public double CLOSE_PRICE_INDEX { get; set; }
        public double OPEN_PRICE_INDEX { get; set; }
        public decimal LastTradePrice { get; set; }
        public double HIGH_PRICE_INDEX { get; set; }
        public double LOW_PRICE_INDEX { get; set; }
        public double BID_SIZE_INDEX { get; set; }
        public double ASK_SIZE_INDEX { get; set; }
        public double LAST_SIZE_INDEX { get; set; }
        public double VOLUME_SIZE_INDEX { get; set; }
        public double FUTURES_OPEN_INTEREST_INDEX { get; set; }
        public double AVG_OPT_VOLUME_INDEX { get; set; }
        public double SHORTABLE_SHARES_INDEX { get; set; }

        public decimal TruePrice
        {
            get
            {
                return (Ask + Bid + LastTradePrice) / 3;
            }
        }
    }
}
