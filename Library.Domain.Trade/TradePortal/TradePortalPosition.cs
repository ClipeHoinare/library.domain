﻿namespace Library.Domain.TradePortal
{

    public class TradePortalPosition : TradePortalAbstract
    {
        public string Account { set; get; }
        public decimal Quantity { get; set; }
        public double AverageCost { get; set; }
        public string Exchange { get; set; }
        public string PrimaryExch { get; set; }
        public string LocalSymbol { get; set; }
        public string Currency { get; set; }
        public string Right { get; set; }
        public double Strike { get; set; }
        public string LastTradeDateOrContractMonth { get; set; }
        public string SecType { get; set; }
        public string Symbol { get; set; }
        public int ConId { get; set; }
        public string TradingClass { get; set; }
    }
}
