﻿namespace Library.Domain.TradePortal
{
    public partial class TradePortalOptionChain : TradePortalAbstract
    {
        public int ConId { get; set; }
        public string Symbol { get; set; }
        public string SecType { get; set; }
        public string Currency { get; set; }
        public string Exchange { get; set; }
        public string Right { get; set; }
        public decimal Ask { get; set; }
        public bool AskFired { get; set; }
        public decimal Bid { get; set; }
        public bool BidFired { get; set; }
        public string LastTradeDateOrContractMonth { get; set; }
        public decimal Strike { get; set; }
        public bool StrikeFired { get; set; }
        public double ImpliedVolatility { get; set; }
        public double Delta { get; set; }
        public bool DeltaFired { get; set; }
        public double Gamma { get; set; }
        public double Vega { get; set; }
        public double Theta { get; set; }
        public string Provider { get; set; }

        public decimal TruePrice
        {
            get
            {
                return (Ask + Bid) / 2;
            }
        }
        public void SetContractMonth(long year, long month, long day)
        {
            LastTradeDateOrContractMonth = year.ToString("0000") + month.ToString("00") + day.ToString("00");
        }
    }
}