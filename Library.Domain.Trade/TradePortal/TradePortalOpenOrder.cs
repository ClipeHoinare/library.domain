﻿using Library.Domain.Infrastructure;

namespace Library.Domain.TradePortal
{
    public class TradePortalOpenOrder : TradePortalAbstract, ICastable
    {
        private TradePortalContract contract;
        private TradePortalOrder order;
        private TradePortalOrderState orderState;

        protected int orderId;

        //public TradePortalOpenOrder(int orderId, TradePortalContract contract, TradePortalOrder order, TradePortalOrderState orderState)
        //{
        //    OrderId = orderId;
        //    Contract = contract;
        //    Order = order;
        //    OrderState = orderState;
        //}

        public int OrderId
        {
            get { return orderId; }
            set { orderId = value; }
        }

        public TradePortalContract Contract
        {
            get { return contract; }
            set { contract = value; }
        }

        public TradePortalOrder Order
        {
            get { return order; }
            set { order = value; }
        }

        public TradePortalOrderState OrderState
        {
            get { return orderState; }
            set { orderState = value; }
        }
    }

    public class TradePortalOrderState
    {
        //public OrderState();
        //public OrderState(string status, string initMarginBefore, string maintMarginBefore, string equityWithLoanBefore, string initMarginChange, string maintMarginChange, string equityWithLoanChange, string initMarginAfter, string maintMarginAfter, string equityWithLoanAfter, double commission, double minCommission, double maxCommission, string commissionCurrency, string warningText);

        public double MaxCommission { get; set; }
        public double MinCommission { get; set; }
        public double Commission { get; set; }
        public string EquityWithLoanAfter { get; set; }
        public string MaintMarginAfter { get; set; }
        public string InitMarginAfter { get; set; }
        public string EquityWithLoanChange { get; set; }
        public string MaintMarginChange { get; set; }
        public string InitMarginChange { get; set; }
        public string EquityWithLoanBefore { get; set; }
        public string MaintMarginBefore { get; set; }
        public string InitMarginBefore { get; set; }
        public string Status { get; set; }
        public string CommissionCurrency { get; set; }
        public string WarningText { get; set; }

        //public override bool Equals(object other);
    }
}
