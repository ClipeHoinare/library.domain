﻿namespace Library.Domain.TradePortal
{
    public class TradePortalAccount : TradePortalAbstract
    {
        public string AccountName { get; set; }
        public string Currency { get; set; }
        public string AccountType { get; set; }
        public string NetLiquidation { get; set; }
        public string TotalCashValue { get; set; }
        public string SettledCash { get; set; }
        public string AccruedCash { get; set; }
        public string BuyingPower { get; set; }
        public string EquityWithLoanValue { get; set; }
        public string PreviousEquityWithLoanValue { get; set; }
        public string GrossPositionValue { get; set; }
        public string ReqTEquity { get; set; }
        public string ReqTMargin { get; set; }
        public string SMA { get; set; }
        public string InitMarginReq { get; set; }
        public string MaintMarginReq { get; set; }
        public string AvailableFunds { get; set; }
        public string ExcessLiquidity { get; set; }
        public string Cushion { get; set; }
        public string FullInitMarginReq { get; set; }
        public string FullMaintMarginReq { get; set; }
        public string FullAvailableFunds { get; set; }
        public string FullExcessLiquidity { get; set; }
        public string LookAheadNextChange { get; set; }
        public string LookAheadInitMarginReq { get; set; }
        public string LookAheadMaintMarginReq { get; set; }
        public string LookAheadAvailableFunds { get; set; }
        public string LookAheadExcessLiquidity { get; set; }
        public string HighestSeverity { get; set; }
        public string DayTradesRemaining { get; set; }
        public string Leverage { get; set; }

    }
}
