﻿using Library.Domain.Jackdow;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Domain.Infrastructure
{
    public interface IImporter<T,S>
    {
        // startTime & endTime should be inclusive
        Task<IReadOnlyList<T>> ImportAsync(S parameters);
        Tuple<IReadOnlyList<T>, Exception>  ImportSync(S parameters);
    }
}
