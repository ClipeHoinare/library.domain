﻿using Library.Domain.Jackdow;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Domain.Infrastructure
{
    public interface IImporterInter<T,S>
    {
        bool AddParameter(S parameter);
        Tuple<T, Exception> GetOneToProcess();
        Tuple<IReadOnlyList<T>, Exception> GetDataLeft();
        Tuple<IReadOnlyList<T>, Exception> ImportSync(S parameters);

    }
}
