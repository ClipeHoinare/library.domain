﻿using Library.Domain.Jackdow;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Domain.Infrastructure
{
    public interface IImporterAsync<T,S>
    {
        // startTime & endTime should be inclusive
        Task<IReadOnlyList<T>> ImportAsync(S parameters);
    }
}
