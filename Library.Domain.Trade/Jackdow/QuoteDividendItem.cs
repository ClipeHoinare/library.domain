﻿using Library.Domain.Infrastructure;
using System;
using System.Collections.Generic;

namespace Library.Domain.Jackdow
{
    public class QuoteDividendItem : ITick
    {

        /// <summary>
        /// The date of the dividend.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public System.DateTime DividendDate { get; set; }

        /// <summary>
        /// The value of the dividend.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal DividendValue { get; set; }

        /// <summary>
        /// The date when the item was created/downloaded.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public System.DateTime DateCreated { get; set; }

        public DateTimeOffset DateTime { get; }

        public QuoteDividendItem()
        {
        }

        public QuoteDividendItem(DateTime dateTime, decimal value)
        {
            DividendDate = dateTime;
            DividendValue = value;
        }
    }

    public class QuoteDividendItemChain : List<QuoteDividendItem> //, ISettableID
    {

        private string mID = string.Empty;
        /// <summary>
        /// The ID of the historic quotes owning stock, index, etc.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ID
        {
            get { return mID; }
        }
        public void SetID(string id)
        {
            mID = id;
        }

        public QuoteDividendItemChain()
            : base()
        {
        }
        public QuoteDividendItemChain(string id)
            : base()
        {
            mID = id;
        }
        public QuoteDividendItemChain(IEnumerable<QuoteDividendItem> items)
            : base()
        {
            foreach (QuoteDividendItem i in items)
            {
                base.Add(i);
            }
            base.Sort(new QuoteDividendItemSorter());
        }

        public QuoteDividendItemChain(string id, IEnumerable<QuoteDividendItem> items)
            : this(items)
        {
            mID = id;
        }

        private class QuoteDividendItemSorter : IComparer<QuoteDividendItem>
        {
            public int Compare(QuoteDividendItem x, QuoteDividendItem y)
            {
                return DateTime.Compare(x.DividendDate, y.DividendDate);
            }
        }

    }

}
