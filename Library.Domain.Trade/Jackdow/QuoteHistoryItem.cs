﻿using Library.Domain.Infrastructure;
using System;
using System.Collections.Generic;

namespace Library.Domain.Jackdow
{
    public class QuoteHistoryItem : ITick
    {
        /// <summary>
        /// The startdate of the period.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public System.DateTime TradingDate { get; set; }
        /// <summary>
        /// The first value in trading period.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal Open { get; set; }
        /// <summary>
        /// The highest value in trading period.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal High { get; set; }
        /// <summary>
        /// The lowest value in trading period.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal Low { get; set; }
        /// <summary>
        /// The last value in trading period.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal Close { get; set; }
        /// <summary>
        /// The last value in trading period in relation to share splits.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal CloseAdjusted { get; set; }
        /// <summary>
        /// The traded volume.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal Volume { get; set; }
        /// <summary>
        /// The close value of the previous HistQuoteData in chain.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public decimal PreviousClose { get; set; }

        public DateTimeOffset DateTime { get; }


        public QuoteHistoryItem()
        {
        }
        public QuoteHistoryItem(DateTime dateTime, decimal open, decimal high, decimal low, decimal close, decimal volume, decimal closeAdjusted)
        {
            TradingDate = dateTime;
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
            CloseAdjusted = closeAdjusted;
        }
    }

    public class QuoteHistoryItemsChain : List<QuoteHistoryItem> //, ISettableID
    {

        private string mID = string.Empty;
        /// <summary>
        /// The ID of the historic quotes owning stock, index, etc.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ID
        {
            get { return mID; }
        }
        public void SetID(string id)
        {
            mID = id;
        }

        public QuoteHistoryItemsChain()
            : base()
        {
        }
        public QuoteHistoryItemsChain(string id)
            : base()
        {
            mID = id;
        }
        public QuoteHistoryItemsChain(IEnumerable<QuoteHistoryItem> items)
            : base()
        {
            foreach (QuoteHistoryItem i in items)
            {
                base.Add(i);
            }
            base.Sort(new QuoteHistoryItemsSorter());
        }

        public QuoteHistoryItemsChain(string id, IEnumerable<QuoteHistoryItem> items)
            : this(items)
        {
            mID = id;
        }

        private class QuoteHistoryItemsSorter : IComparer<QuoteHistoryItem>
        {
            public int Compare(QuoteHistoryItem x, QuoteHistoryItem y)
            {
                return DateTime.Compare(x.TradingDate, y.TradingDate);
            }
        }

    }

}
