﻿using System;

namespace Library.Domain.Infrastructure
{
    public interface ITick
    {
        DateTimeOffset DateTime { get; }
    }
}
