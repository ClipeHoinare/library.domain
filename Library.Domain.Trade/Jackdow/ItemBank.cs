﻿using System.Collections.Generic;

namespace Library.Domain.Jackdow
{
    public class ItemBank
    {
        public List<FlipFlopCommand> Commands = new List<FlipFlopCommand>();
    }


    public interface IModelTable
    {

    }

    public enum ModelTableType
    {
        Both = 0,
        Insert = 1,
        Update = 2
    }
}
