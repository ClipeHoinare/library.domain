﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Domain.Jackdow
{
    public enum PeriodOption
    {
        PerSecond,
        PerMinute,
        Per5Minute,
        Per10Minute,
        Per15Minute,
        Per30Minute,
        Hourly,
        BiHourly,
        Daily,
        Weekly,
        Monthly
    }
}