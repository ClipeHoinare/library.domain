﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Library.Domain.Jackdow
{
    public class FlipFlopCommand
    {
        public int MicActivityID = 0;
        public int ParentMicActivityID = 0;
        public string Symbol;
        public int MicCommandID = 0;
        public DateTime FromDateTime = DateTime.MinValue;
        public DateTime ToDateTime = DateTime.MinValue;

        /// <summary>
        /// Price 
        /// </summary>
        public double PriceStart;
        public double GroundBase;
        public double Delta = 0;
        public double Clip;
        public double PriceFinal = 0;
        public DateTime FinalDateTime = DateTime.MinValue;
        public double LastTradePriceOnly = 0;
        public DateTime LastTradeTime = DateTime.MinValue;
        public int Active = 0;
        public int Processed = 0;
        public int Enabled = 0;
        public int Logged = 0;
    }
}
