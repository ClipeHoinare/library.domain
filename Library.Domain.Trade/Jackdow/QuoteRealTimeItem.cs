﻿using Library.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Library.Domain.Jackdow
{
    public class QuoteRealTimeItem : ITick
    {
        public DateTime TradingDateTime { get; set; }
        public int TradingDate { get; set; }
        public int TradingTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTimeOffset DateTime { get; }
        public int SecId { get; protected set; }
        public string Symbol { get; protected set; }
        public string SecType { get; protected set; }
        public double Ask { get; set; }
        public double Bid { get; set; }
        public double Close { get; set; }
        public double Delta { get; set; }
        public double Gamma { get; set; }
        public double High { get; set; }
        public double Impl_vol { get; set; }
        public double Last { get; set; }
        public double Low { get; set; }
        public double Mark { get; set; }
        public double Open { get; set; }
        public double Strike { get; set; }
        public double Theta { get; set; }
        public double Vega { get; set; }
        public double Volume { get; set; }


        public QuoteRealTimeItem(string symbol, int secId, string secType)
        {
            this.Symbol = symbol;
            this.SecId = secId;
            this.SecType = secType;
        }
    }
}
