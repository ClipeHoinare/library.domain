﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Domain.Trade.Base
{
    public class FinanceParameter
    {
        public int SecId;
        public string Symbol;
        private string secType;
        public DateTime StartDateTime;
        public DateTime? EndDateTime;
        public int Timeout;
        /// <summary>
        /// IDX, ETF, STK, OPT
        /// </summary>
        /// <value>IDX, ETF, STK, OPT</value>
        public string SecType { get => secType; set => secType = value; }
    }
}
