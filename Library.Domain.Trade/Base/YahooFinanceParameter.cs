﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Wrapper.Trady.Yahoo
{
    public class YahooFinanceParameter
    {
        public string symbol;
        public DateTime? startTime;
        public DateTime? endTime;
    }
}
