﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Library.Domain.Core.Configuration
{

    public class AppSettings
    {

        public string SandboxConsumerKey = ConfigurationManager.AppSettings["SANDBOX_CONSUMER_KEY"];
        public string SandboxConsumerSecret = ConfigurationManager.AppSettings["SANDBOX_CONSUMER_SECRET"];
        public string LiveConsumerKey = ConfigurationManager.AppSettings["LIVE_CONSUMER_KEY"];
        public string LiveConsumerSecret = ConfigurationManager.AppSettings["LIVE_CONSUMER_SECRET"];
        public string EtradeUser = ConfigurationManager.AppSettings["USER"];
        public string EtradePass = ConfigurationManager.AppSettings["PASSWORD"];

        public AppSettings()
        {
        }
    }

    public class ApplicationConfiguration
    {
        public static AppSettings AppSettings { get; set; }

        static ApplicationConfiguration()
        {
            /// Load the properties from the Config store
            AppSettings = new AppSettings();
        }
    }
}
