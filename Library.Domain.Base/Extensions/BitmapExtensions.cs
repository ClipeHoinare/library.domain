﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;

namespace Library.Domain.Base.Extensions
{
    public static class BitmapExtensions
    {
        ///// <summary>
        ///// See if bmp is contained in template with a small margin of error.
        ///// </summary>
        ///// <param name="template">The Bitmap that might contain.</param>
        ///// <param name="bmp">The Bitmap that might be contained in.</param>        
        ///// <returns>You guess!</returns>
        //public static bool Contains(this Bitmap template, Bitmap bmp)
        //{
        //    const Int32 divisor = 4;
        //    const Int32 epsilon = 10;

        //    ExhaustiveTemplateMatching etm = new ExhaustiveTemplateMatching(0.9f);

        //    TemplateMatch[] tm = etm.ProcessImage(
        //        new ResizeNearestNeighbor(template.Width / divisor, template.Height / divisor).Apply(template),
        //        new ResizeNearestNeighbor(bmp.Width / divisor, bmp.Height / divisor).Apply(bmp)
        //        );

        //    if (tm.Length == 1)
        //    {
        //        Rectangle tempRect = tm[0].Rectangle;

        //        if (Math.Abs(bmp.Width / divisor - tempRect.Width) < epsilon
        //            &&
        //            Math.Abs(bmp.Height / divisor - tempRect.Height) < epsilon)
        //        {
        //            return true;
        //        }
        //    }

        //    return false;
        //}


        /// <summary>
        /// Find the location of a bitmap within another bitmap and return if it was successfully found
        /// </summary>
        /// <param name="bmpNeedle">The image we want to find</param>
        /// <param name="bmpHaystack">Where we want to search for the image</param>
        /// <param name="location">Where we found the image</param>
        /// <returns>If the bmpNeedle was found successfully</returns>
        public static bool FindBitmap(this Bitmap bmpHaystack, Bitmap bmpNeedle, out Point location)
        {
            if (bmpNeedle == null || bmpHaystack == null)
            {
                location = new Point();
                return false;
            }
            for (int outerX = 0; outerX <= bmpHaystack.Width - bmpNeedle.Width; outerX++)
            {
                for (int outerY = 0; outerY <= bmpHaystack.Height - bmpNeedle.Height; outerY++)
                {
                    for (int innerX = 0; innerX < bmpNeedle.Width; innerX++)
                    {
                        for (int innerY = 0; innerY < bmpNeedle.Height; innerY++)
                        {
                            Color cNeedle = bmpNeedle.GetPixel(innerX, innerY);
                            Color cHaystack = bmpHaystack.GetPixel(innerX + outerX, innerY + outerY);

                            if (cNeedle.R != cHaystack.R || cNeedle.G != cHaystack.G || cNeedle.B != cHaystack.B)
                            {
                                goto notFound;
                            }
                        }
                    }
                    location = new Point(outerX, outerY);
                    return true;
                    notFound:
                    continue;
                }
            }
            location = Point.Empty;
            return false;
        }

        /// <summary>
        /// //http://csharpexamples.com/c-fast-bitmap-compare/
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static bool CompareBitmapsFast(this Bitmap bitmap, Bitmap second)
        {
            if (bitmap == null || second == null)
                return false;
            if (object.Equals(bitmap, second))
                return true;
            if (!bitmap.Size.Equals(second.Size) || !bitmap.PixelFormat.Equals(second.PixelFormat))
                return false;

            int bytes = bitmap.Width * bitmap.Height * (Image.GetPixelFormatSize(bitmap.PixelFormat) / 8);

            bool result = true;
            byte[] b1bytes = new byte[bytes];
            byte[] b2bytes = new byte[bytes];

            BitmapData bitmapData1 = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            BitmapData bitmapData2 = second.LockBits(new Rectangle(0, 0, second.Width, second.Height), ImageLockMode.ReadOnly, second.PixelFormat);

            Marshal.Copy(bitmapData1.Scan0, b1bytes, 0, bytes);
            Marshal.Copy(bitmapData2.Scan0, b2bytes, 0, bytes);

            for (int n = 0; n <= bytes - 1; n++)
            {
                if (b1bytes[n] != b2bytes[n])
                {
                    result = false;
                    break;
                }
            }

            bitmap.UnlockBits(bitmapData1);
            second.UnlockBits(bitmapData2);

            return result;
        }

    }
}
