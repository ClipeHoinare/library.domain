﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Library.Domain.Base.Extensions
{
    public static class DateTimeExtensions
    {
        public static GregorianCalendar _gc = new GregorianCalendar();
        public static int GetWeekOfMonth(this DateTime dt)
        {
            DateTime first = new DateTime(dt.Year, dt.Month, 1);
            return dt.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }
        public static int GetWeekOfYear(this DateTime dt)
        {
            return _gc.GetWeekOfYear(dt, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }
        public static int GetDayOfWeek(this DateTime dt)
        {
            return (int)dt.DayOfWeek;
        }
        public static int GetWeeksInMonth(this DateTime dt)
        {
           var weeksOfMount = GetWeekOfMonth(GetEndOfMonth(dt));

            return weeksOfMount;
        }
        public static DateTime GetEndOfMonth(this DateTime dt)
        {
            DateTime endOfMonth = new DateTime(
                dt.Year,                                               
                dt.Month,                                               
                DateTime.DaysInMonth(dt.Year, dt.Month));

            return endOfMonth;
        }
    }
}
