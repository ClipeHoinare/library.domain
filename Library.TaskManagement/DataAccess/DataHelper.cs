﻿using System.Data;
using System.Data.SqlClient;
using TaskManagement.TaskSpooler.Common;

namespace TaskManagement.TaskSpooler.DataAccess
{
    public class DataHelper
    {
        #region Properties

        private int commandTimeout = 900;
        public int CommandTimeout
        {
            get { return commandTimeout; }
            set { commandTimeout = value; }
        }

        private string databaseName = "TriadDen";
        public string DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        #endregion

        #region Constructor

        public DataHelper()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet GetApplicationKeys()
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetConfiguration");
            cmd.CommandTimeout = CommandTimeout;

            return db.ExecuteDataSet(cmd);     
        }
        
        #endregion
    }
}
