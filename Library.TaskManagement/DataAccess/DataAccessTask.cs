﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;
using TaskManagement.TaskSpooler.Common;

namespace TaskManagement.TaskSpooler.DataAccess
{
    public class DataAccessTask
    {
        #region Properties

        private int commandTimeout = 900;
        public int CommandTimeout
        {
            get { return commandTimeout; }
            set { commandTimeout = value; }
        }

        private string databaseName = "TriadDen";
        public string DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        #endregion

        #region Constructor

        public DataAccessTask()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="command"></param>
        /// <param name="data"></param>
        public int AddTaskToQueue(int command, string parameters, string userName, int objectType, int parentQueueId)
        {
            int queueId = 0;

            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueAddToQueue");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@TaskQueueCommandId", SqlDbType.Int, command);
            db.AddInParameter(cmd, "@parameters", SqlDbType.VarChar, parameters);
            db.AddInParameter(cmd, "@UserName", SqlDbType.VarChar, userName);
            db.AddInParameter(cmd, "@ObjectType", SqlDbType.Int, objectType);
            db.AddInParameter(cmd, "@ParentQueueId", SqlDbType.Int, parentQueueId);

            object obj = db.ExecuteScalar(cmd);

            if (obj != null)
                queueId = Convert.ToInt32(obj.ToString());

            return queueId;
        }


        public DataSet GetTaskQueue(int taskQueueId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetSqlStringCommand(" select TaskQueueId,TaskQueueCommandId,TaskQueueServerId, " +
                "CreateDate,TaskQueueStatusId, Data, " +
                "Priority,StartDate,EndDate from taskqueue " +
                " where TaskQueueId=" + taskQueueId.ToString());
            cmd.CommandTimeout = CommandTimeout;

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public DataSet GetTaskURL(bool principal)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("GetTaskURL");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@Principal", SqlDbType.Bit, principal ? 1 : 0);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public DataSet TaskGetNextQueueIdTimestamp(string serverName, int taskId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetNextQueueIdTimeStamp");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@TaskQueueServerName", SqlDbType.VarChar, 50, serverName);
            db.AddInParameter(cmd, "@TaskQueueThreadId", SqlDbType.Int, taskId);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskQueueId"></param>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public DataSet RunTaskFromQueue(int taskQueueId, byte[] timeStamp)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueRunFromQueue");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@TaskQueueId", SqlDbType.Int, taskQueueId);
            db.AddInParameter(cmd, "@AccessDate", SqlDbType.Binary, 8, timeStamp);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverName"></param>
        /// <returns></returns>
        public DataSet GetTaskServerThread(string serverName)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetTaskQueueServerThread");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ServerName", SqlDbType.VarChar, serverName);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        public void RollbackTask(int taskQueueId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetSqlStringCommand("update TaskQueue " +
                " set TaskQueueServerId=null, StartDate=null, EndDate=null, " +
                " TaskQueueStatusId=1, RunAfterDate=DATEADD(minute,5,getdate()) where TaskQueueId=" + taskQueueId.ToString());
            cmd.CommandTimeout = CommandTimeout;
            db.ExecuteNonQuery(cmd);
        }

        public void ResendTask(int taskQueueId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetSqlStringCommand("update TaskQueue " +
                " set TaskQueueServerId=null, StartDate=null, EndDate=null, " +
                " TaskQueueStatusId=1 where TaskQueueId=" + taskQueueId.ToString());
            cmd.CommandTimeout = CommandTimeout;
            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskQueueId"></param>
        public void UpdateTaskStatus(int taskQueueId, int taskQueueStatusId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueUpdateStatus");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@TaskQueueId", SqlDbType.Int, taskQueueId);
            db.AddInParameter(cmd, "@TaskQueueStatusId", SqlDbType.Int, taskQueueStatusId);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="importQueueId"></param>
        /// <param name="exportQueueId"></param>
        /// <returns></returns>
        public bool IsPreviewTaskReady(int[] commands)
        {
            bool ready = false;

            string xlmdata = "<?xml version=\"1.0\"?><catalog>";
            foreach (int command in commands)
            {
                xlmdata = xlmdata + "<book id=\"" + command.ToString() + "\"><ID>" + command.ToString() + "</ID></book>";
            }
            xlmdata = xlmdata + "</catalog>";

            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueuePreviewReady");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@xmldata", SqlDbType.Xml, new SqlXml(new XmlTextReader(xlmdata
                           , XmlNodeType.Document, null)));

            object obj = db.ExecuteScalar(cmd);

            if (obj != null)
                ready = Convert.ToInt32(obj.ToString()) == 1;

            return ready;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool VerifyServer(string name)
        {
            bool found = false;

            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueVerifyServer");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@Name", SqlDbType.VarChar, 50, name);

            object obj = db.ExecuteScalar(cmd);

            if (obj != null)
                found = Convert.ToInt32(obj.ToString()) > 0;

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet GetScheduleList()
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetScheduleList");
            cmd.CommandTimeout = CommandTimeout;
            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public DataSet GetSchedule(int scheduleId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("GetTaskQueueSchedule");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ScheduleId", SqlDbType.VarChar, scheduleId);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        public void SaveSchedule(int scheduleId, int taskCommandId, bool enabled, string startDate, string startTime,
            int objectType, bool sunday, bool monday, bool tuesday, bool wednesday, bool thursday, bool friday, bool saturday, string param)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueSaveSchedule");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@TaskQueueScheduleId", SqlDbType.Int, scheduleId);
            db.AddInParameter(cmd, "@TaskQueueCommandId", SqlDbType.Int, taskCommandId);
            db.AddInParameter(cmd, "@Enabled", SqlDbType.Int, enabled ? 1 : 0);
            db.AddInParameter(cmd, "@StartDate", SqlDbType.VarChar, 20, startDate);
            db.AddInParameter(cmd, "@StartTime", SqlDbType.VarChar, 20, startTime);
            db.AddInParameter(cmd, "@Sunday", SqlDbType.Int, sunday ? 1 : 0);
            db.AddInParameter(cmd, "@Monday", SqlDbType.Int, monday ? 1 : 0);
            db.AddInParameter(cmd, "@Tuesday", SqlDbType.Int, tuesday ? 1 : 0);
            db.AddInParameter(cmd, "@Wednesday", SqlDbType.Int, wednesday ? 1 : 0);
            db.AddInParameter(cmd, "@Thursday", SqlDbType.Int, thursday ? 1 : 0);
            db.AddInParameter(cmd, "@Friday", SqlDbType.Int, friday ? 1 : 0);
            db.AddInParameter(cmd, "@Saturday", SqlDbType.Int, saturday ? 1 : 0);

            if (param != null)
                db.AddInParameter(cmd, "@Param", SqlDbType.VarChar, param);

            db.AddInParameter(cmd, "@ObjectType", SqlDbType.Int, objectType);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleId"></param>
        public void DeleteSchedule(int scheduleId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueDeleteSchedule");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ScheduleId", SqlDbType.VarChar, scheduleId);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTaskCommandId"></param>
        /// <param name="searchServerId"></param>
        /// <param name="searchStatusId"></param>
        /// <param name="searchUserName"></param>
        /// <param name="searchObjectType"></param>
        /// <param name="searchRunDate"></param>
        /// <param name="searchCreateDate"></param>
        /// <param name="taskCommandId"></param>
        /// <param name="taskServerId"></param>
        /// <param name="statusId"></param>
        /// <param name="userName"></param>
        /// <param name="objectType"></param>
        /// <param name="runDate"></param>
        /// <param name="createDate"></param>
        /// <returns></returns>
        public DataSet GetTaskQueueList(bool searchTaskCommandId, bool searchServerId, bool searchStatusId,
            bool searchUserName, bool searchObjectType, bool searchRunDate, bool searchCreateDate, int taskCommandId,
            int taskServerId, int statusId, string userName, int objectType, string runDate, string createDate)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetQueueList");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@SearchTaskCommandId", SqlDbType.Int, searchTaskCommandId ? 1 : 0);
            db.AddInParameter(cmd, "@SearchServerId", SqlDbType.Int, searchServerId ? 1 : 0);
            db.AddInParameter(cmd, "@SearchStatusId", SqlDbType.Int, searchStatusId ? 1 : 0);
            db.AddInParameter(cmd, "@SearchUserName", SqlDbType.Int, searchUserName ? 1 : 0);
            db.AddInParameter(cmd, "@SearchObjectType", SqlDbType.Int, searchObjectType ? 1 : 0);
            db.AddInParameter(cmd, "@SearchRunDate", SqlDbType.Int, searchRunDate ? 1 : 0);
            db.AddInParameter(cmd, "@SearchCreateDate", SqlDbType.Int, searchCreateDate ? 1 : 0);
            db.AddInParameter(cmd, "@TaskQueueCommandId", SqlDbType.Int, taskCommandId);
            db.AddInParameter(cmd, "@TaskQueueServerId", SqlDbType.Int, taskServerId);
            db.AddInParameter(cmd, "@StatusId", SqlDbType.Int, statusId);

            if (userName != null)
                db.AddInParameter(cmd, "@UserName", SqlDbType.VarChar, userName);

            db.AddInParameter(cmd, "@ObjectType", SqlDbType.Int, objectType);

            if (runDate != null)
                db.AddInParameter(cmd, "@RunDate", SqlDbType.VarChar, runDate);

            if (createDate != null)
                db.AddInParameter(cmd, "@CreateDate", SqlDbType.VarChar, createDate);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskQueueId"></param>
        public void DeleteTaskQueue(int taskQueueId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueDeleteQueueId");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@TaskQueueId", SqlDbType.Int, taskQueueId);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 
        /// </summary>
        public void CleanupTaskQueue()
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueCleanupQueue");
            cmd.CommandTimeout = CommandTimeout;

            db.ExecuteNonQuery(cmd);
        }

        public void LogThreadState(string serverName, int managedThreadId, int taskQueueId, int itemId, DateTime currentDate, bool StartDate, string comment)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueLogThreadState");
            db.AddInParameter(cmd, "@ServerName", SqlDbType.VarChar, serverName);
            db.AddInParameter(cmd, "@ManagedThreadId", SqlDbType.Int, managedThreadId);
            db.AddInParameter(cmd, "@TaskQueueId", SqlDbType.Int, taskQueueId);
            db.AddInParameter(cmd, "@ItemId", SqlDbType.Int, itemId);
            db.AddInParameter(cmd, "@CurrentDate", SqlDbType.VarChar, currentDate.ToShortDateString() + " " + currentDate.ToShortTimeString());
            db.AddInParameter(cmd, "@Comment", SqlDbType.VarChar, comment);

            cmd.CommandTimeout = CommandTimeout;

            db.ExecuteNonQuery(cmd);
        }

        public DataSet TaskQueueGetNextTaskScheduledIdandTimestamp()
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetNextTaskScheduledIdandTimestamp");
            cmd.CommandTimeout = CommandTimeout;

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        public DataSet TaskQueueGetScheduleTaskByIDandTimestamp(int scheduleId, byte[] timeStamp)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetScheduleTaskByIDandTimestamp");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ScheduleId", SqlDbType.Int, scheduleId);
            db.AddInParameter(cmd, "@AccessDate", SqlDbType.Binary, 8, timeStamp);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        public void UpdateScheduleNextRunTime(int scheduleId, DateTime NextRunTime)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueUpdateScheduleNextRunTime");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ScheduleId", SqlDbType.Int, scheduleId);
            db.AddInParameter(cmd, "@NextRunDate", SqlDbType.DateTime, NextRunTime);

            db.ExecuteNonQuery(cmd);
        }

        #endregion

        public DataSet TaskQueueGetTaskScheduledIdsByCommand(int taskQueueCommandId)
        {
            Database db = DatabaseFactory.CreateDatabase(DatabaseName);

            SqlCommand cmd = db.GetStoredProcCommand("TaskQueueGetTaskScheduledIdsByCommand");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@taskQueueCommandId", SqlDbType.Int, taskQueueCommandId);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }
    }
}
