using System;
using System.Data;
using System.Data.SqlClient;

namespace TaskManagement.TaskSpooler.Common
{
    /// <summary>
    /// This class provides the common functionality for the data accessor objects.
    /// </summary>
    public abstract class AbstractDataAccessor : IDisposable
    {
        #region private properties

        private string _connectionString;
        protected System.Data.SqlClient.SqlTransaction _trans;
        protected System.Data.SqlClient.SqlConnection _con;
        protected bool _closeConnectionWithTrans;

        #endregion

        #region public properties
        /// <summary>
        /// Sql connection object.
        /// </summary>
        internal System.Data.SqlClient.SqlConnection Con
        {
            get { return _con; }
            set { _con = value; }
        }

        /// <summary>
        /// Sql transaction object.
        /// </summary>
        internal System.Data.SqlClient.SqlTransaction Trans
        {
            get { return _trans; }
            set { _trans = value; }
        }

        /// <summary>
        /// This flag indicates whether or not the connection
        /// was opened when a transaction was started.  If it
        /// was, this flag will tell the commit and rollback
        /// functions to close the connection.
        /// </summary>
        internal bool CloseConnectionWithTrans
        {
            get { return _closeConnectionWithTrans; }
            set { _closeConnectionWithTrans = value; }
        }

        /// <summary>
        /// Connection string for the sql connection object
        /// </summary>
        internal string ConnectionString
        {
            get { return _connectionString; }
            set
            {
                _connectionString = value;
                GetRegistryConnectionString(ref _connectionString);
                Con = new System.Data.SqlClient.SqlConnection(_connectionString);
            }
        }

        #endregion

        #region constructor/destructor

        /// <summary>
        /// Creates and initializes the abstract data accessor.
        /// </summary>
        /// <param name="connectionKey">Database connection string to use.</param>
        public AbstractDataAccessor(string connectionKey)
            : this(connectionKey, 0, false)
        {
        }

        /// <summary>
        /// Creates and initializes the abstract data accessor.
        /// </summary>
        /// <param name="connectionString">Database connection string to use.</param>
        /// <param name="type">0 if connection is connectionKey; 
        /// 1 if connection is  connectionString.</param>
        public AbstractDataAccessor(string connection, int type)
            : this(connection, type, false)
        {
        }

        /// <summary>
        /// Creates and initializes the abstract data accessor.
        /// </summary>
        /// <param name="connectionString">Database connection string to use.</param>
        /// <param name="type">connectionKey if 0 otherwise connectionString.</param>
        /// <param name="keepAlive">True if the connection should be held open
        /// across multiple calls to the data object.  NOTE: If you set this flag,
        /// you MUST call the Dispose method, otherwise the connection will be held
        /// open until the garbage collection cleans up this object.</param>
        public AbstractDataAccessor(string connection, int type, bool keepAlive)
        {
            // set connection
            if (type == 0)
                ConnectionString = GetConnectionString(connection);
            else
                ConnectionString = connection;

            // Open the connection
            //open connection
            if (keepAlive)
            {
                Con.Open();
            }

            // Initialize the close flag
            CloseConnectionWithTrans = false;
        }

        /// <summary>
        /// This method is called by the garbage collector to free the object.
        /// </summary>
        ~AbstractDataAccessor()
        {
            // Clean up references
            FreeReferences();
        }

        /// <summary>
        /// Performs application-defined tasks associated with 
        /// freeing, releasing, or resetting unmanaged resources.
        /// </summary>		
        public virtual void Dispose()
        {
            // Free all references
            FreeReferences();

            // Suppress the finalize
            GC.SuppressFinalize(this);
        }

        public void GetRegistryConnectionString(ref string connectionString)
        {
            if (connectionString != null && connectionString.ToLower().StartsWith("registry:hklm\\") && connectionString.IndexOf(",") >= 0)
            {
                string regKey = connectionString.Substring(14, connectionString.LastIndexOf(",") - 14);
                string regValue = connectionString.Substring(connectionString.LastIndexOf(",") + 1);
                Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(regKey, false);
                connectionString = (string)rk.GetValue(regValue);
            }
        }

        /// <summary>
        /// Returns the connection string form connectionStrings in web.config
        /// </summary>
        /// <param name="connectionKey"></param>
        /// <returns></returns>
        private string GetConnectionString(string connectionKey)
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings[connectionKey].ConnectionString;
        }

        /// <summary>
        /// This method performs the cleanup for the class.
        /// </summary>
        private void FreeReferences()
        {
            // Get rid of the connection and the transaction
            if (Trans != null)
            {
                try
                {
                    // Rollback by default - commits should be explicit
                    Trans.Rollback();
                }
                catch (Exception)
                {
                    // Do nothing.
                }
                Trans.Dispose();
                Trans = null;
            }
            if (Con != null)
            {
				try
				{
					if (Con.State != System.Data.ConnectionState.Closed)
					{
						Con.Close();
					}
					// mCon.Dispose ();  Don't call dispose on the connection!!!!!!
					Con = null;
				}
				catch (Exception)
				{
					// Do nothoing.
				}
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Begins a database transaction.
        /// </summary>
        /// <param name="iso">The isolation level under which the transaction should run.</param>
        public void BeginTransaction(System.Data.IsolationLevel iso)
        {
            try
            {
                // Check the connection, it has to be open to start a transaction
                if (Con.State != System.Data.ConnectionState.Open)
                {
                    // Open the connection
                    Con.Open();

                    // Set the flag that will close the connection when the 
                    // transaction is committed or rolled back
                    CloseConnectionWithTrans = true;
                }

                // Create the transaction
                Trans = Con.BeginTransaction(iso);
            }
            catch (Exception ex)
            {
                throw new Exception("An exception was thrown while starting the database transaction.", ex);
            }
        }

        /// <summary>
        /// Executes the SqlCommand object and returns no records.
        /// </summary>
        /// <param name="cmd">Command to execute.</param>
        protected virtual int ExecuteNonQuery(System.Data.SqlClient.SqlCommand cmd)
        {
            // Execute the command, don't use a transaction
            return ExecuteNonQuery(cmd, false);
        }

        /// <summary>
        /// Executes the SqlCommand object and returns no records.
        /// </summary>
        /// <param name="cmd">Command to execute.</param>
        /// <param name="participateInTransaction">True is this call should participate in the transaction, if available.</param>
        protected virtual int ExecuteNonQuery(System.Data.SqlClient.SqlCommand cmd, bool participateInTransaction)
        {
            bool openedLocally = false;
            int returnCode = -1;
            try
            {
                // Set the command
                cmd.Connection = this.Con;

                // Set the transaction
                if (participateInTransaction)
                {
                    // Set the transaction object
                    cmd.Transaction = this.Trans;
                }

                // Check to see if the connection is open, if it isn't open it
                if (Con.State != System.Data.ConnectionState.Open)
                {
                    Con.Open();
                    openedLocally = true;
                }

                // Execute the query
                returnCode = cmd.ExecuteNonQuery();

                // If the connection was opened locally, close it
                if (openedLocally)
                {
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to execute [{0}]", this.SerializeSqlObject(cmd)), ex);
            }
            return returnCode;
        }

        /// <summary>
        /// Executes the given command and returns an object.
        /// </summary>
        /// <param name="cmd">Command to execute.</param>
        /// <returns>object.</returns>
        protected virtual object ExecuteScalar(System.Data.SqlClient.SqlCommand cmd)
        {
            object obj = null;
            try
            {
                // Set the command
                cmd.Connection = this._con;

                // Check to see if the connection is open, if it isn't open it
                if (_con.State != System.Data.ConnectionState.Open)
                {
                    _con.Open();
                }

                // Execute the query
                obj = cmd.ExecuteScalar();

                // If the connection was opened locally, close it
                _con.Close();
            }
            catch (Exception ex)
            {
                if (_con.State == System.Data.ConnectionState.Open)
                {
                    _con.Close();
                }
                throw new Exception(string.Format("Failed to create a SQL reader for [{0}]", this.SerializeSqlObject(cmd)), ex);
            }
            return obj;
        }

        /// <summary>
        /// Executes the given command and returns a SqlDataReader.
        /// </summary>
        /// <param name="cmd">Command to execute.</param>
        /// <returns>SqlDataReader object.</returns>
        protected virtual System.Data.SqlClient.SqlDataReader ExecuteReader(System.Data.SqlClient.SqlCommand cmd)
        {
            return ExecuteReader(cmd, System.Data.CommandBehavior.Default);
        }

        /// <summary>
        /// Executes the given command and returns a SqlDataReader.
        /// </summary>
        /// <param name="cmd">Command to execute.</param>
        /// <param name="behavior">Provides a description of the results of the query and its effect on the database.</param>
        /// <returns>SqlDataReader object.</returns>
        protected virtual System.Data.SqlClient.SqlDataReader ExecuteReader(System.Data.SqlClient.SqlCommand cmd, System.Data.CommandBehavior behavior)
        {

            try
            {
                // Set the command
                cmd.Connection = this.Con;

                // Check to see if the connection is open, if it isn't open it
                if (Con.State != System.Data.ConnectionState.Open)
                {
                    Con.Open();

                    // We opened the connection, but we can't close it when the 
                    // function returns because the data reader needs an open connection
                    // to read.  Instead, set the flag in the behavior to tell ADO.NET to 
                    // automatically close the connection when the reader is closed.
                    behavior = behavior | System.Data.CommandBehavior.CloseConnection;
                }

                // Execute the query
                return cmd.ExecuteReader(behavior);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to create a SQL reader for [{0}]", this.SerializeSqlObject(cmd)), ex);
            }
        }

        /// <summary>
        /// Creates and fills a DataSet using the given command.
        /// </summary>
        /// <param name="cmd">Command to use when filling the DataSet.</param>
        /// <returns>DataSet filled by the given command.</returns>
        protected System.Data.DataSet FillDataSet(System.Data.SqlClient.SqlCommand cmd)
        {
            System.Data.DataSet ds = null;
            try
            {
                // Create the dataset to be filled
                ds = new System.Data.DataSet();

                // Call a helper method to fill the DataSet
                FillDataSet(ds, null, cmd);
            }
            catch (Exception)
            {
                // Null out the dataset
                ds = null;

                // Re-throw the exception
                throw;
            }
            return ds;
        }

        /// <summary>
        /// Fills a given DataSet using the given command.
        /// </summary>
        /// <param name="ds">DataSet to fill.</param>
        /// <param name="cmd">Command to use when filling the DataSet.</param>
        protected void FillDataSet(System.Data.DataSet ds, System.Data.SqlClient.SqlCommand cmd)
        {
            try
            {
                // Make sure the DataSet is initialized
                if (ds == null)
                {
                    throw new ArgumentNullException("ds", "The DataSet was not provided.");
                }

                FillDataSet(ds, null, cmd);
            }
            catch (Exception)
            {
                // Re-throw the exception
                throw;
            }
        }

        /// <summary>
        /// Fills a given DataSet using the given command, with the given table name 
        /// </summary>
        /// <param name="ds">DataSet to fill.</param>
        /// <param name="tableName">Name to give new table in DataSet.</param>
        /// <param name="cmd">Command to use when filling the DataSet</param>
        protected void FillDataSet(System.Data.DataSet ds, string tableName, System.Data.SqlClient.SqlCommand cmd)
        {
            System.Data.SqlClient.SqlDataAdapter da = null;
            bool openedLocally = false;

            try
            {
                // Make sure the DataSet is initialized
                if (ds == null)
                {
                    throw new ArgumentNullException("ds", "The DataSet was not provided.");
                }

                // Set the command
                cmd.Connection = this.Con;

                // Set the transaction
                cmd.Transaction = this.Trans;

                // Check to see if the connection is open, if it isn't open it
                if (Con.State != System.Data.ConnectionState.Open)
                {
                    Con.Open();
                    openedLocally = true;
                }

                // Create the data adapter and fill the dataset
                da = new SqlDataAdapter(cmd);

                // Fill with the table name if it is available
                if (tableName != null && tableName != System.String.Empty)
                {
                    da.Fill(ds, tableName);
                }
                else
                {
                    da.Fill(ds);
                }
                da.Dispose();
                da = null;

                // If the connection was opened locally, close it
                if (openedLocally)
                {
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                // Re-throw the exception
                throw new Exception(string.Format("Failed to fill dataset [{0}]", this.SerializeSqlObject(cmd)), ex);
            }
            finally
            {
                if (da != null)
                {
                    da.Dispose();
                    da = null;
                }
            }
        }

        /// <summary>
        /// Commits the database transaction.
        /// </summary>
        public void CommitTransaction()
        {
            try
            {
                Trans.Commit();

                // Clean up the transaction
                Trans.Dispose();
                Trans = null;

                // If the flag indicates, close the connection as well
                if (CloseConnectionWithTrans)
                {
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to commit the transaction.", ex);
            }
        }

        /// <summary>
        /// Rolls the transaction back.
        /// </summary>
        public void RollbackTransaction()
        {
            try
            {
                Trans.Rollback();

                // Clean up the transaction
                Trans.Dispose();
                Trans = null;

                // If the flag indicates, close the connection as well
                if (CloseConnectionWithTrans)
                {
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to rollback the transaction.", ex);
            }
        }

        /// <summary>
        /// Serializes the given command information for error 
        /// reporting purposes.
        /// </summary>
        /// <param name="cmd">Command to serialize.</param>
        /// <returns>String containing the information on the given command.</returns>
        protected string SerializeSqlObject(System.Data.SqlClient.SqlCommand cmd)
        {
            System.Text.StringBuilder serializedCommand = new System.Text.StringBuilder();

            try
            {
                // Make sure a command was passed in.
                if (cmd == null)
                {
                    return null;
                }

                // Add the command text
                serializedCommand.Append(cmd.CommandText + ":");

                // Loop through the parameters
                for (int i = 0; i < cmd.Parameters.Count; i++)
                {
                    serializedCommand.Append(string.Format("\n<{0}>: '{1}'", cmd.Parameters[i].ParameterName, cmd.Parameters[i].Value));
                }

                // Return the serialized command
                return serializedCommand.ToString();
            }
            catch (Exception)
            {
                // Ignore the error
                // This method will probably be called in the
                // catch method of data accessors.  The last
                // thing we need is to be throwing exceptions
                // from our catch statement.
                return null;
            }
            finally
            {
                // Clean up
                serializedCommand = null;
            }
        }

        protected void AddInParameter(SqlCommand cmd, string parameterName, DbType dbType, object parameterValue)
        {
            AddParameter(cmd, parameterName, dbType, 0, ParameterDirection.Input, parameterValue);
        }

        protected void AddInParameter(SqlCommand cmd, string parameterName, DbType dbType, int size, object parameterValue)
        {
            AddParameter(cmd, parameterName, dbType, size, ParameterDirection.Input, parameterValue);
        }

        protected void AddOutParameter(SqlCommand cmd, string parameterName, DbType dbType, int size)
        {
            AddParameter(cmd, parameterName, dbType, size, ParameterDirection.Output, DBNull.Value);
        }

        protected void AddParameter(SqlCommand cmd, string parameterName, DbType dbType, int size, ParameterDirection direction, object parameterValue)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = parameterName;
            param.DbType = dbType;
            param.Size = size;
            param.Direction = direction;
            param.Value = (parameterValue == null) ? DBNull.Value : parameterValue;
            cmd.Parameters.Add(param);
        }

		protected void AddInParameter(SqlCommand cmd, string parameterName, SqlDbType sqlDbType, object parameterValue)
		{
			AddParameter(cmd, parameterName, sqlDbType, 0, ParameterDirection.Input, parameterValue);
		}

		protected void AddInParameter(SqlCommand cmd, string parameterName, SqlDbType sqlDbType, int size, object parameterValue)
		{
			AddParameter(cmd, parameterName, sqlDbType, size, ParameterDirection.Input, parameterValue);
		}

		protected void AddOutParameter(SqlCommand cmd, string parameterName, SqlDbType sqlDbType, int size)
		{
			AddParameter(cmd, parameterName, sqlDbType, size, ParameterDirection.Output, DBNull.Value);
		}

		protected void AddParameter(SqlCommand cmd, string parameterName, SqlDbType sqlDbType, int size, ParameterDirection direction, object parameterValue)
		{
			SqlParameter param = new SqlParameter();
			param.ParameterName = parameterName;
			param.SqlDbType = sqlDbType;
			param.Size = size;
			param.Direction = direction;
			param.Value = (parameterValue == null) ? DBNull.Value : parameterValue;
			cmd.Parameters.Add(param);
		}

		protected void SetParameterValue(SqlCommand cmd, string parameterName, object parameterValue)
        {
            cmd.Parameters[parameterName].Value = (parameterValue == null) ? DBNull.Value : parameterValue;
        }

        protected SqlCommand GetStoredProcCommand(string text)
        {
            SqlCommand cmd = new SqlCommand(text);
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }

        protected SqlCommand GetSqlStringCommand(string text)
        {
            SqlCommand cmd = new SqlCommand(text);
            cmd.CommandType = CommandType.Text;
            return cmd;
        }
        #endregion
    }
}
