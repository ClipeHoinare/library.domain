using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Text;

namespace TaskManagement.TaskSpooler.Common
{
	public class OracleDatabase : IDisposable
	{
		#region private properies

		private string _connectionKey = string.Empty;
		private string _connectionString = string.Empty;
		private OracleConnection _connection = null;

		/// <summary>
		/// OracleConnection for the object
		/// </summary>
		public OracleConnection Connection
		{
			get 
			{
				if (_connection == null || _connection.ConnectionString != _connectionString)
				{
					_connection = new OracleConnection(_connectionString);
					_connectionString = _connection.ConnectionString;
				}
				return _connection; 
			}
			set { _connection = value; }
		}


		#endregion

		#region public properies

		/// <summary>
		/// Connection string 
		/// </summary>
		public string ConnectionString
		{
			get { return _connectionString; }
			set { _connectionString = value; }
		}

		#endregion

		#region constructor/destructor

		/// <summary>
		/// Creates and initializes the OracleDatabase.
		/// </summary>
		public OracleDatabase(string connectionKey)
		{
			_connectionKey = connectionKey;
			_connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[_connectionKey].ConnectionString;
		}

        /// <summary>
        /// This method is called by the garbage collector to free the object.
        /// </summary>
		~OracleDatabase()
        {
            // Clean up references
            FreeReferences();
        }

		/// <summary>
		/// Performs application-defined tasks associated with 
		/// freeing, releasing, or resetting unmanaged resources.
		/// </summary>		
		public virtual void Dispose()
		{
			// Free all references
			FreeReferences();

			// Suppress the finalize
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// This method performs the cleanup for the class.
		/// </summary>
		private void FreeReferences()
		{
			try
			{
				// Get rid of the connection 
				if (Connection != null)
				{
					if (Connection.State != System.Data.ConnectionState.Closed)
					{
						Connection.Close();
					}
					// Connection.Dispose ();  Don't call dispose on the connection!!!!!!
					Connection = null;
				}
			}
			catch(Exception)
			{
				// Do nothing.
			}
		}

		#endregion

		#region public methods

		public OracleCommand GetSqlStringCommand(string text)
		{
			OracleCommand command = new OracleCommand(text);
			command.CommandType = CommandType.Text;
			return command;
		}

		public DataSet ExecuteDataSet(OracleCommand cmd)
		{
			DataSet ds = new DataSet();
			
			cmd.Connection = Connection;
			Connection.Open();
			try
			{
				OracleDataAdapter da = new OracleDataAdapter(cmd);
				da.Fill(ds);
			}
			catch (Exception ex)
			{
				Connection.Close();
				throw ex;
			}
			return ds;

		}

		#endregion
	}
}
