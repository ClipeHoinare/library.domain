using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TaskManagement.TaskSpooler.Common
{
    public class DatabaseFactory
    {
        public static Database CreateDatabase(string connectionKey)
        {
            Database db = new Database(connectionKey);
            return db;
        }

        //public static OracleDatabase CreateOracleDatabase(string connectionKey)
        //{
        //    OracleDatabase db = new OracleDatabase(connectionKey);
        //    return db;
        //}
	}
    public class Database : TaskSpooler.Common.AbstractDataAccessor
    {
        public Database(string connectionKey)
            : base(connectionKey)
        {
        }
        public new SqlCommand GetStoredProcCommand(string text)
        {
            return base.GetStoredProcCommand(text);
        }
        public new SqlCommand GetSqlStringCommand(string text)
        {
            return base.GetSqlStringCommand(text);
        }
        public new void AddInParameter(SqlCommand cmd, string parameterName, DbType dbType, int size, object parameterValue)
        {
            base.AddInParameter(cmd, parameterName, dbType, size, parameterValue);
        }
        public new void AddInParameter(SqlCommand cmd, string parameterName, DbType dbType, object parameterValue)
        {
            base.AddInParameter(cmd, parameterName, dbType, parameterValue);
        }
		public new void AddInParameter(SqlCommand cmd, string parameterName, SqlDbType dbType, int size, object parameterValue)
		{
			base.AddInParameter(cmd, parameterName, dbType, size, parameterValue);
		}
		public new void AddInParameter(SqlCommand cmd, string parameterName, SqlDbType dbType, object parameterValue)
		{
			base.AddInParameter(cmd, parameterName, dbType, parameterValue);
		}
		public DataSet ExecuteDataSet(SqlCommand cmd)
        {
            return base.FillDataSet(cmd);
        }
        public new void ExecuteNonQuery(SqlCommand cmd)
        {
            base.ExecuteNonQuery(cmd);
        }
        public new SqlDataReader ExecuteReader(SqlCommand cmd)
        {
            return base.ExecuteReader(cmd);
        }
        public new object ExecuteScalar(System.Data.SqlClient.SqlCommand cmd)
        {
            return base.ExecuteScalar(cmd);
        }
    }
}
