﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TaskManagement.TaskSpooler.DataAccess;

namespace TaskManagement.TaskSpooler.Common
{
    public class ApplicationKey
    {
        #region Properties

        int _spoolerTimeInterval = 5000;
        public int SpoolerTimeInterval
        {
            get { return _spoolerTimeInterval; }
            set { _spoolerTimeInterval = value; }
        }

        int _spoolerScheduledTimeInterval = 5;
        public int SpoolerScheduledTimeInterval
        {
            get { return _spoolerScheduledTimeInterval; }
            set { _spoolerScheduledTimeInterval = value; }
        }

        int _spoolerQueueCleanup = 6;
        public int SpoolerQueueCleanup
        {
            get { return _spoolerQueueCleanup; }
            set { _spoolerQueueCleanup = value; }
        }

        #endregion

        #region Constructor

        public ApplicationKey()
        {
        }

        #endregion

        #region Public Methods

        public void LoadApplicationKeys()
        {
            try
            {
                DataHelper dh = new DataHelper();
                DataSet ds = dh.GetApplicationKeys();

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];

                        if (dr["configKey"].ToString().Equals("SpoolerTimeInterval"))
                        {
                            SpoolerTimeInterval = Convert.ToInt32(dr["configValue"].ToString());
                        }
                        else if (dr["configKey"].ToString().Equals("SpoolerScheduledTimeInterval"))
                        {
                            SpoolerScheduledTimeInterval = Convert.ToInt32(dr["configValue"].ToString());
                        }
                        else if (dr["configKey"].ToString().Equals("SpoolerQueueCleanup"))
                        {
                            SpoolerQueueCleanup = Convert.ToInt32(dr["configValue"].ToString());
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion
    }
}
