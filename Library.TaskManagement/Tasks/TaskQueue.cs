﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.TaskSpooler.DataAccess;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace TaskManagement.TaskSpooler.Tasks
{
    public class TaskQueue
    {
        #region Properties

        private int taskQueueId = 0;
        public int TaskQueueId
        {
            get { return taskQueueId; }
            set { taskQueueId = value; }
        }

        private byte[] accessDate = null;
        public byte[] TimeStamp
        {
            get { return accessDate; }
            set { accessDate = value; }
        }

        private string data = String.Empty;
        public string Data
        {
            get { return data; }
            set { data = value; }
        }

        private TaskHelper.eTaskCommand taskCommandId = 0;
        public TaskHelper.eTaskCommand TaskCommandId
        {
            get { return taskCommandId; }
            set { taskCommandId = value; }
        }

        private string userName = String.Empty;
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private TaskHelper.eTaskObjectType objectType = 0;
        public TaskHelper.eTaskObjectType ObjectType
        {
            get { return objectType; }
            set { objectType = value; }
        }

        private string parameters;
        public string Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }


        #endregion

        #region Constructor

        public TaskQueue()
        {

        }

        public TaskQueue(int id)
        {
            TaskQueueId = id;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="command"></param>
        /// <param name="data"></param>
        /// <param name="userName"></param>
        /// <param name="objectType"></param>
        /// <param name="parentQueueId"></param>
        /// <returns></returns>
        public int Add(TaskHelper.eTaskCommand command, string parameters, string userName, TaskHelper.eTaskObjectType objectType, int parentQueueId)
        {
            DataAccessTask dataAccess = new DataAccessTask();
            return dataAccess.AddTaskToQueue((int)command, parameters, userName, (int)objectType, parentQueueId);
        }



        public DataSet GetTaskQueueDataset()
        {
            DataAccessTask dataAccess = new DataAccessTask();
            return dataAccess.GetTaskQueue(TaskQueueId);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public bool GetNextTask(string serverName, TaskHelper.ThreadEnum taskId)
        {
            GetNextTaskIdTimeStamp(serverName, taskId);

            if (TaskQueueId > 0)
            {
                RunTaskFromQueue(TaskQueueId, TimeStamp);
                return true;
            }

            return false;
        }

        // Resend the task to be execute again.
        public void ResendTask()
        {
            DataAccessTask dataAccess = new DataAccessTask();
            dataAccess.ResendTask(TaskQueueId);
        }

        // Rollback the task to be execute again.
        public void RollbackTask()
        {
            DataAccessTask dataAccess = new DataAccessTask();
            dataAccess.RollbackTask(TaskQueueId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskStatusId"></param>
        public void UpdateTaskStatus(TaskHelper.eTaskStatus taskStatusId)
        {
            DataAccessTask dataAccess = new DataAccessTask();
            dataAccess.UpdateTaskStatus(TaskQueueId, (int)taskStatusId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="taskId"></param>
        private void GetNextTaskIdTimeStamp(string serverName, TaskHelper.ThreadEnum taskId)
        {
            TaskQueueId = 0;

            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.TaskGetNextQueueIdTimestamp(serverName, (int)taskId);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                TaskQueueId = dr.IsNull("TaskQueueId") ? 0 : Convert.ToInt32(dr["TaskQueueId"].ToString());
                if (!dr.IsNull("AccessDate"))
                {
                    TimeStamp = (byte[])dr["AccessDate"];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskQueueId"></param>
        /// <param name="timeStamp"></param>
        private void RunTaskFromQueue(int taskQueueId, byte[] timeStamp)
        {
            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.RunTaskFromQueue(taskQueueId, timeStamp);

            TaskQueueId = 0;

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                TaskQueueId = dr.IsNull("TaskQueueId") ? 0 : Convert.ToInt32(dr["TaskQueueId"].ToString());
                TaskCommandId = dr.IsNull("TaskQueueCommandId") ? TaskHelper.eTaskCommand.Unknown : (TaskHelper.eTaskCommand)Convert.ToInt32(dr["TaskQueueCommandId"].ToString());
                UserName = dr.IsNull("UserName") ? null : dr["UserName"].ToString();
                //Data = dr.IsNull("Data") ? null : dr["Data"].ToString();
                ObjectType = dr.IsNull("ObjectType") ? TaskHelper.eTaskObjectType.Unknown : (TaskHelper.eTaskObjectType)Convert.ToInt32(dr["ObjectType"].ToString());

                Parameters = dr.IsNull("Parameters") ? null : dr["Parameters"].ToString();

                if (!dr.IsNull("AccessDate"))
                {
                    TimeStamp = (byte[])dr["AccessDate"];
                }
            }
        }

        public bool IsPreviousTaskReady(TaskHelper.eTaskCommand[] eTaskCommands)
        {
            DataAccessTask dataTask = new DataAccessTask();
            return dataTask.IsPreviewTaskReady(Array.ConvertAll(eTaskCommands, value => (int) value));
        }

        public DataSet GetQueueList()
        {
            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.GetTaskQueueList(false, false, false,
            false, false, false, false, 0,
            0, 0, null, 0, null, null);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTaskCommandId"></param>
        /// <param name="searchServerId"></param>
        /// <param name="searchStatusId"></param>
        /// <param name="searchUserName"></param>
        /// <param name="searchObjectType"></param>
        /// <param name="searchRunDate"></param>
        /// <param name="searchCreateDate"></param>
        /// <param name="taskCommandId"></param>
        /// <param name="taskServerId"></param>
        /// <param name="statusId"></param>
        /// <param name="userName"></param>
        /// <param name="objectType"></param>
        /// <param name="runDate"></param>
        /// <param name="createDate"></param>
        /// <returns></returns>
        public DataSet GetQueueList(bool searchTaskCommandId, bool searchServerId, bool searchStatusId,
            bool searchUserName, bool searchObjectType, bool searchRunDate, bool searchCreateDate, int taskCommandId,
            int taskServerId, int statusId, string userName, int objectType, string runDate, string createDate)
        {
            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.GetTaskQueueList(searchTaskCommandId, searchServerId, searchStatusId,
            searchUserName, searchObjectType, searchRunDate, searchCreateDate, taskCommandId,
            taskServerId, statusId, userName, objectType, runDate, createDate);

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Delete()
        {
            DataAccessTask dataTask = new DataAccessTask();
            dataTask.DeleteTaskQueue(TaskQueueId);
        }

        /// <summary>
        /// 
        /// </summary>
        public void CleanupTaskQueue()
        {
            DataAccessTask dataTask = new DataAccessTask();
            dataTask.CleanupTaskQueue();
        }

        #endregion
    }
}
