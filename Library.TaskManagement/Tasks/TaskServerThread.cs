﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskManagement.TaskSpooler.Tasks
{
    public class TaskServerThread
    {
        #region Properties

        private int taskServerThreadId = 0;
        public int TaskServerThreadId
        {
            get { return taskServerThreadId; }
            set { taskServerThreadId = value; }
        }

        private int taskServerId = 0;
        public int TaskServerId
        {
            get { return taskServerId; }
            set { taskServerId = value; }
        }

        private TaskHelper.ThreadEnum threadId = TaskHelper.ThreadEnum.Unknown;
        public TaskHelper.ThreadEnum ThreadId
        {
            get { return threadId; }
            set { threadId = value; }
        }

        private int threadCount = 0;
        public int ThreadCount
        {
            get { return threadCount; }
            set { threadCount = value; }
        }

        private int currentThreadCount = 0;
        public int CurrentThreadCount
        {
            get { return currentThreadCount; }
            set { currentThreadCount = value; }
        }

        #endregion

        #region Constructor

        public TaskServerThread()
        {
        }

        #endregion
    }
}
