﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Net;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using TaskManagement.TaskSpooler.DataAccess;
using System.Collections;

namespace TaskManagement.TaskSpooler.Tasks
{
    public class TaskHelper
    {
        public enum ThreadEnum
        {
            Unknown = 0,
            Scheduled = 1,
            Sequential = 2,
            Harmony = 3
        }
        /// <summary>
        /// task status enumeration
        /// </summary>
        public enum eTaskStatus
        {
            Unknown = 0,
            InQueue = 1,
            Taken = 2,
            Running = 3,
            Completed = 4,
            Failed = 5,
            Cancelled = 6,
            Terminated = 7
        }

        public enum eTaskObjectType
        {
            Unknown = 0,
            SessionID = 1,
            PPSStatus = 2,
            TestEventID = 3,
            InventoryCheck = 4
        }

        public enum eParameter
        {
            Unknown = 0,
            New,
            NewVersion,
            Rejected
        }

        public enum eTaskCommand
        {
            Unknown = 0,
            GetStocksInfo = 1,
            FillHistoryFromLastDay = 2,
            DashboardOutput = 3,
            GetStocksHistoryFillEmptySpots = 4,
            SuspendSystem = 5, 
            CleanupTaskQueue = 21,
            LoadConfig = 22,
            Custom = 28,
            CustomManual = 29,
            ResearchBuildSignals = 31
        }

        public TaskHelper()
        {
        }

        public string SendTaskAlert()
        {
            return SendTaskAlert(false);
        }

        public bool VerifyServer(string name)
        {
            DataAccessTask dataAccess = new DataAccessTask();
            return dataAccess.VerifyServer(name);
        }

        public SortedList GetTaskServerThread(string serverName)
        {
            SortedList slTaskServerThread = new SortedList();

            DataAccessTask dataAccess = new DataAccessTask();
            DataSet ds = dataAccess.GetTaskServerThread(serverName);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    TaskServerThread taskServerThreat = new TaskServerThread();

                    DataRow dr = ds.Tables[0].Rows[i];

                    taskServerThreat.TaskServerThreadId = dr.IsNull("TaskQueueServerThreadId") ? 0 : Convert.ToInt32(dr["TaskQueueServerThreadId"].ToString());
                    taskServerThreat.TaskServerId = dr.IsNull("TaskQueueServerId") ? 0 : Convert.ToInt32(dr["TaskQueueServerId"].ToString());
                    taskServerThreat.ThreadId = dr.IsNull("TaskQueueThreadId") ? 0 : (TaskHelper.ThreadEnum)Convert.ToInt32(dr["TaskQueueThreadId"].ToString());
                    taskServerThreat.ThreadCount = dr.IsNull("ThreadCount") ? 0 : Convert.ToInt32(dr["ThreadCount"].ToString());

                    slTaskServerThread.Add(taskServerThreat.ThreadId,  taskServerThreat);
                }
            }

            return slTaskServerThread;
        }

        public string SendTaskAlert(bool principal)
        {
            string returnedXmlString = "";

            try
            {
                DataAccessTask dataAccess = new DataAccessTask();
                DataSet ds = dataAccess.GetTaskURL(principal);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {                    
                    string url = ds.Tables[0].Rows[i]["TaskURL"].ToString();
                    string clienIP = "";// HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    string data = "RequestCommand=taskalert&" + "&clientIP=" + clienIP;

                    byte[] buff = SitePost(url, data);

                    returnedXmlString = System.Text.ASCIIEncoding.ASCII.GetString(buff);
                }
            }
            catch (Exception ex)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<xml></xml>");
                XmlNode docNode = doc.DocumentElement;
                XmlElement errorElement = doc.CreateElement("error");
                XmlAttribute errorAttribute = doc.CreateAttribute("text");
                errorAttribute.Value = ex.Message + ex.StackTrace;
                errorElement.Attributes.Append(errorAttribute);
                docNode.AppendChild(errorElement);

                returnedXmlString = doc.OuterXml;
            }


            return (returnedXmlString);
        }

        private byte[] SitePost(string url, string data)
        {
            byte[] retBuff = new byte[0];
            Exception exToThrow = null;
            HttpWebRequest wr = null;
            HttpWebResponse ws = null;
            try
            {
                //prepare request
                Uri uri = new Uri(url);

                wr = (HttpWebRequest)WebRequest.Create(uri);
                wr.Timeout = 55000;
                wr.Accept = "*/*";
                wr.AllowWriteStreamBuffering = false;
                wr.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                wr.Method = "POST";
                wr.ContentType = "application/x-www-form-urlencoded";
                wr.KeepAlive = true;

                wr.UserAgent = "MSIE 6.0";

                byte[] sendBuff = System.Text.ASCIIEncoding.ASCII.GetBytes(data);
                wr.ContentLength = sendBuff.Length;
                Stream outStream = wr.GetRequestStream();
                outStream.Write(sendBuff, 0, sendBuff.Length);
                int len = 0;
                outStream.Flush();
                outStream.Close();

                //get response
                ws = (HttpWebResponse)wr.GetResponse();

                // read response
                Stream str = ws.GetResponseStream();
                MemoryStream ms = new MemoryStream();
                retBuff = new byte[8192];

                len = str.Read(retBuff, 0, retBuff.Length);
                while (len > 0)
                {
                    ms.Write(retBuff, 0, len);
                    len = str.Read(retBuff, 0, retBuff.Length);
                }
                str.Close();
                retBuff = ms.ToArray();

                ms.Close();
            }
            catch (Exception ex)
            {
                exToThrow = ex;
            }

            // clean after you
            try
            {
                if (ws != null)
                    ws.Close();
            }
            catch { }

            try
            {
                if (wr != null)
                    wr.Abort();
            }
            catch { }

            // if error encounter throw it again
            if (exToThrow != null)
                throw exToThrow;

            return (retBuff);
        }

        public void LogThreadState(string serverName, int managedThreadId, int taskQueueId, int itemId, DateTime currentDate, bool startDate, string comment)
        {
            try
            {
                DataAccessTask daTask = new DataAccessTask();
                daTask.LogThreadState(serverName, managedThreadId, taskQueueId, itemId, currentDate, startDate, comment);
            }
            catch(Exception)
            {

            }
        }
        
        
       
    }
}
