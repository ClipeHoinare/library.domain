﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using TaskManagement.TaskSpooler.DataAccess;

namespace TaskManagement.TaskSpooler.Tasks
{
    public class TaskSchedule
    {
        #region Properties

        private int scheduleId = 0;
        public int ScheduleId
        {
            get { return scheduleId; }
            set { scheduleId = value; }
        }

        private DateTime lastRunTime = DateTime.MinValue;
        public DateTime LastRunTime
        {
            get { return lastRunTime; }
            set { lastRunTime = value; }
        }

        private DateTime nextRunTime = DateTime.MinValue;
        public DateTime NextRunTime
        {
            get { return nextRunTime; }
            set { nextRunTime = value; }
        }

        private string param = String.Empty;
        public string Parameters
        {
            get { return param; }
            set { param = value; }
        }

        private byte[] accessDate = null;
        public byte[] TimeStamp
        {
            get { return accessDate; }
            set { accessDate = value; }
        }

        public Dictionary<DayOfWeek, bool> WeekEnable = new Dictionary<DayOfWeek, bool>();  

        private bool enabled = false;
        public bool Enable
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public bool ExecuteIfDayPass { get; set; }

        private TaskHelper.eTaskCommand taskCommandId = 0;
        public TaskHelper.eTaskCommand TaskCommandId
        {
            get { return taskCommandId; }
            set { taskCommandId = value; }
        }

        private string startTime = String.Empty;
        public string StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        private string startDate = String.Empty;
        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        private TaskHelper.eTaskObjectType objType = TaskHelper.eTaskObjectType.Unknown;
        public TaskHelper.eTaskObjectType ObjType
        {
            get { return objType; }
            set { objType = value; }
        }


        #endregion
        
        #region Constructor

        public TaskSchedule()
        {
        }

        #endregion

        #region public methods


        public bool GetNextTask()
        {
            GetNextTaskIdTimeStamp();

            if (ScheduleId > 0)
            {
                GetScheduleTaskByIDandTimestamp(ScheduleId, TimeStamp);

                if (ScheduleId > 0)
                {
                    //HACK: two servers would have an issue with this, need to rework latter
                    UpdateScheduleNextRunDate();

                    ValidateIfCanBeRunNow();

                    if (ScheduleId > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void GetNextTaskIdTimeStamp()
        {
            ScheduleId = 0;

            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.TaskQueueGetNextTaskScheduledIdandTimestamp();

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                ScheduleId = dr.IsNull("TaskQueueScheduleId") ? 0 : Convert.ToInt32(dr["TaskQueueScheduleId"].ToString());
                TimeStamp = dr.IsNull("AccessDate") ? null : (byte[])dr["AccessDate"];
            }
        }

        private void GetScheduleTaskByIDandTimestamp(int taskQueueScheduleId, byte[] timeStamp)
        {
            ScheduleId = 0;

            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.TaskQueueGetScheduleTaskByIDandTimestamp(taskQueueScheduleId, timeStamp);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                ScheduleId = Convert.ToInt32(dr["TaskQueueScheduleId"].ToString());
                TaskCommandId = (TaskHelper.eTaskCommand)Convert.ToInt32(dr["TaskQueueCommandId"].ToString());
                LastRunTime = Convert.ToDateTime(dr["LastRunDate"].ToString());
                NextRunTime = Convert.ToDateTime(dr["NextRunDate"].ToString());
                Parameters = dr["Parameters"].ToString();
                TimeStamp = (byte[])dr["AccessDate"];

                Enable = Convert.ToBoolean(dr["Enable"]);
                ExecuteIfDayPass = Convert.ToBoolean(dr["ExecuteIfDayPass"]);

                WeekEnable[DayOfWeek.Sunday] = Convert.ToBoolean(dr["Sunday"]);
                WeekEnable[DayOfWeek.Monday] = Convert.ToBoolean(dr["Monday"]);
                WeekEnable[DayOfWeek.Tuesday] = Convert.ToBoolean(dr["Tuesday"]);
                WeekEnable[DayOfWeek.Wednesday] = Convert.ToBoolean(dr["Wednesday"]);
                WeekEnable[DayOfWeek.Thursday] = Convert.ToBoolean(dr["Thursday"]);
                WeekEnable[DayOfWeek.Friday] = Convert.ToBoolean(dr["Friday"]);
                WeekEnable[DayOfWeek.Saturday] = Convert.ToBoolean(dr["Saturday"]);
            }
        }

        public void UpdateScheduleNextRunDate()
        {
            // push it for next day no time to calculate now the next real run
            // need to create a better schedule system
            DateTime futureDatetime = NextRunTime.AddDays(1);
            DataAccessTask dataAccess = new DataAccessTask();
            dataAccess.UpdateScheduleNextRunTime(ScheduleId, futureDatetime);
        }

        private void ValidateIfCanBeRunNow()
        {
            DateTime currentDatetime = DateTime.Now;
            if (!(currentDatetime.Date.Equals(NextRunTime.Date) || ExecuteIfDayPass))
                ScheduleId = 0;

            if (!WeekEnable[NextRunTime.DayOfWeek])
                ScheduleId = 0;

            if (!Enable)
                ScheduleId = 0;
        }

        #region unused

        //public static List<TaskSchedule> GetScheduleList()
        //{
        //    DataAccessTask dataTask = new DataAccessTask();
        //    DataSet ds = dataTask.GetScheduleList();

        //    List<TaskSchedule> lts = new List<TaskSchedule>();

        //    if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
        //    {
        //        for(int i=0; i<ds.Tables[0].Rows.Count;i++)
        //        {
        //            DataRow dr = ds.Tables[0].Rows[i];
        //            TaskSchedule ts = new TaskSchedule();

        //            ts.ScheduleId = dr.IsNull("TaskQueueScheduleId") ? 0 : Convert.ToInt32(dr["TaskQueueScheduleId"].ToString());
        //            ts.TaskCommandId = dr.IsNull("TaskCommandId") ? TaskHelper.eTaskCommand.Unknown : (TaskHelper.eTaskCommand)Convert.ToInt32(dr["TaskCommandId"].ToString());
        //            ts.LastRunTime = dr.IsNull("LastRunDate") ? DateTime.Parse("2014-08-01") : Convert.ToDateTime(dr["LastRunDate"].ToString());
        //            ts.NextRunTime = dr.IsNull("NextRunTime") ? DateTime.Parse("2014-08-01") : Convert.ToDateTime(dr["NextRunTime"].ToString());
        //            ts.Parameters = dr.IsNull("Parameters") ? null : dr["Parameters"].ToString();
        //        }
        //    }

        //    return lts;
        //}

        //public void GetSchedule()
        //{
        //    DataAccessTask dataTask = new DataAccessTask();
        //    dataTask.GetSchedule(ScheduleId);
        //}

        //public void Save()
        //{
        //    DataAccessTask dataTask = new DataAccessTask();
        //    dataTask.SaveSchedule(ScheduleId, (int)TaskCommandId, Enable, StartDate, StartTime, (int)ObjType, 
        //        Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Parameters);
        //}

        //public void Delete()
        //{
        //    DataAccessTask dataTask = new DataAccessTask();
        //    dataTask.DeleteSchedule(ScheduleId);
        //}

        #endregion

        #endregion

        public bool PostponeTasksByCommand(TaskHelper.eTaskCommand eTaskCommand)
        {
            bool successfull = true;

            DataAccessTask dataTask = new DataAccessTask();
            DataSet ds = dataTask.TaskQueueGetTaskScheduledIdsByCommand((int)eTaskCommand);

            foreach(DataRow dr in ds.Tables[0].Rows)
            {
                TaskSchedule ts = new TaskSchedule() { 
                    ScheduleId = Convert.ToInt32(dr["TaskQueueScheduleId"])
                    , TimeStamp = dr.IsNull("AccessDate") ? null : (byte[])dr["AccessDate"] 
                };
                successfull = successfull && ts.PostponeTask();
            }

            return successfull;
        }

        private bool PostponeTask()
        {
            if (ScheduleId > 0)
            {
                GetScheduleTaskByIDandTimestamp(ScheduleId, TimeStamp);

                if (ScheduleId > 0)
                {
                    //HACK: two servers would have an issue with this, need to rework latter
                    UpdateScheduleNextRunDate();

                    if (ScheduleId > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
