﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskManagement.TaskSpooler.Tasks
{
    public class Task
    {
        private int taskId = 0;
        public int TaskId
        {
            get { return taskId; }
            set { taskId = value; }
        }

        private byte[] accessDate = null;
        public byte[] TimeStamp
        {
            get { return accessDate; }
            set { accessDate = value; }
        }


        public Task()
        {
        }

    }
}
